package evaluable7;

public class PERSONA {

  private String Nombre;

  private Integer DNI; //Sin letra
  
  public PERSONA() {
    this.Nombre = "";
    this.DNI = 0;
}

  public String getNombre() {
      return this.Nombre;
  }

  public Integer getDNI() {
      return this.DNI;
  }

  public void setNombre(String a) {
      this.Nombre = a;
  }

  public void setDNI(Integer a) {
      this.DNI = a;
  }
  
}