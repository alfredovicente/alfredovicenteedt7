package evaluable7;

public class Acampado extends PERSONA {

  private Integer campamentos_anteriores;
  private Grupo grup = new Grupo();

   /**
   * 
     * @param a
   * @element-type Grupo
   */
  
  public Acampado() {
    this.campamentos_anteriores = 0;
  }

  public void setcampamentos_anteriores(int a) {
      this.campamentos_anteriores = a;
  }

  public Integer getcampamentos_anteriores() {
      return this.campamentos_anteriores;
  }
  
  public Grupo getGrupo() {
      return this.grup;
  }

  public void setGrupo(Grupo a) {
      this.grup = a;
  }
  

}