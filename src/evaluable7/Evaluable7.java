/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evaluable7;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;


/**
 *
 * @author lliurex
 */
public class Evaluable7 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        ArrayList Grupo_educadores = new ArrayList();
        ArrayList Todos_los_acampados = new ArrayList();
        ArrayList Todos_los_grupos = new ArrayList();
        int i;
        //Definimos 3 grupos
        
        Grupo peques = new Grupo();
        peques.setNombre("Pequenos");
        peques.setCaracteristicas("Son los mas pequenos");
        Grupo medianos = new Grupo();
        medianos.setNombre("Medianos");
        medianos.setCaracteristicas("Son los medianos");
        Grupo grandes = new Grupo();
        grandes.setNombre("Grandes");
        grandes.setCaracteristicas("Son los grades");
        Todos_los_grupos.add(peques);
        Todos_los_grupos.add(medianos);
        Todos_los_grupos.add(grandes);

        //Rellenamos los valores. Aquí pondriamos una interfaz para preguntar por los datos
        for (i=0; i<10;i++){
            Acampado ac = new Acampado();
            ac.setNombre("Acampado"+i);
            ac.setDNI(1000000+i);
            ac.setcampamentos_anteriores(i);
            ac.setGrupo(peques);
            Todos_los_acampados.add(ac);
        }
        for (i=10; i<20;i++){
            Acampado ac = new Acampado();
            ac.setNombre("Acampado"+i);
            ac.setDNI(1000000+i);
            ac.setcampamentos_anteriores(i);
            ac.setGrupo(medianos);
            Todos_los_acampados.add(ac);
        }
        for (i=20; i<30;i++){
            Acampado ac = new Acampado();
            ac.setNombre("Acampado"+i);
            ac.setDNI(1000000+i);
            ac.setcampamentos_anteriores(i);
            ac.setGrupo(grandes);
            Todos_los_acampados.add(ac);
        }
        
        for (i=0; i<5;i++){
            Monitor ac = new Monitor();
            ac.setNombre("Educador"+i);
            ac.setDNI(2000000+i);
            switch(i){
                case 0: ac.setGrupo(grandes); break;
                case 1: ac.setGrupo(peques); break;
                case 2: ac.setGrupo(medianos); break;
                case 3: ac.setGrupo(grandes); break;
                case 4: ac.setGrupo(medianos); break;
            }
            ac.setExperiencia(0+i);
            ac.setHorasAsignadas(i+1*10);
            SimpleDateFormat dat = new SimpleDateFormat("1976-07-"+(5+i));
            ac.setfechaAlta(dat);        
            Grupo_educadores.add(ac);
        }
        
        for (i=0; i<5;i++){
            Cocinero ac = new Cocinero();
            ac.setNombre("Cocinero"+i);
            ac.setDNI(3000000+i);
            switch(i){
                case 0: ac.setEspecialidad("Arroces"); break;
                case 1: ac.setEspecialidad("Fideuas"); break;
                case 2: ac.setEspecialidad("CAldos"); break;
                case 3: ac.setEspecialidad("Macarrones"); break;
                case 4: ac.setEspecialidad("Dulces");break;
            }
            ac.setExperiencia(0+i);
            ac.setHorasAsignadas(i+1*10);
            SimpleDateFormat dat = new SimpleDateFormat("1976-07-"+(10+i));
            ac.setfechaAlta(dat);        
            Grupo_educadores.add(ac);
        }
        
        for(Iterator it3 = Todos_los_grupos.iterator(); it3.hasNext();){
            Grupo g = (Grupo)it3.next();
            String gruponombre = g.getNombre();
            System.out.println("\n*********************************************************\nGrupo: " + g.getNombre() + "\nCaracteristicas: " + g.getCaracteristicas()+"\n");
            System.out.println("-------------------EDUCADORES----------------------------\n");
            for(Iterator it2 = Grupo_educadores.iterator(); it2.hasNext();){
                Object aux = it2.next();
                if (aux instanceof Monitor){
                    Monitor m = (Monitor)aux;                
                    if(gruponombre.equals(m.getGrupo().getNombre())){
                        String output = m.getfechaAlta().toPattern();
                        System.out.println("Nombre: " + m.getNombre() + " || TituloMTL: " + m.getTitulo_MTL() + " || TituloAJ: " + m.getTitulo_AJ() + " || Fecha Alta: " + output);
                    }
                }
                
            }
            System.out.println("\n-------------------ACAMPADOS------------------------------\n");
            for (Iterator it = Todos_los_acampados.iterator(); it.hasNext();){
                Acampado p = (Acampado)it.next();
                if (gruponombre.equals(p.getGrupo().getNombre())){
                    System.out.println("Nombre: " + p.getNombre()+" || DNI: "+p.getDNI());
                }
            }
            System.out.println("\n*********************************************************");
        }
        System.out.println("\n-------------------COCINEROS----------------------------");
        for(Iterator it5 = Grupo_educadores.iterator(); it5.hasNext();){
            Object aux2 = it5.next();
            if (aux2 instanceof Cocinero){
                    Cocinero c = (Cocinero)aux2;                
                    String output = c.getfechaAlta().toPattern();
                    System.out.println("Nombre: " + c.getNombre() + " || TituloMTL: " + c.getTitulo_MTL() + " || TituloAJ: " + c.getTitulo_AJ() + " || Fecha Alta: " + output + " || Especialidad: " + c.getEspecialidad());
                } else {}
            }
    }
}
