package evaluable7;


import java.text.SimpleDateFormat;

public class Educadores extends PERSONA {

  private Boolean Titulo_MTL;

  private Boolean Titulo_AJ;

  private Integer Experiencia;

  private Integer HorasAsignadas;

  private SimpleDateFormat fechaAlta;
  
  private Grupo grup = new Grupo();

  
  
  public Educadores () {
    this.Titulo_MTL=false;
    this.Titulo_AJ=false;
    this.Experiencia=0;
    this.HorasAsignadas=0;
    SimpleDateFormat dat1 = new SimpleDateFormat("1970-01-01");
    this.fechaAlta=dat1;
  }

  public Boolean getTitulo_MTL() {
      return this.Titulo_MTL;
  }

  public Boolean getTitulo_AJ() {
      return this.Titulo_AJ;
  }

  public Integer getExperiencia() {
      return this.Experiencia;
  }

  public Integer getHorasAsignadas() {
      return this.HorasAsignadas;
  }

  public void setTitulo_MTL(Boolean a) {
      this.Titulo_MTL = a;
  }

  public void setTitulo_AJ(Boolean a) {
      this.Titulo_AJ = a;
  }

  public void setExperiencia(Integer a) {
      this.Experiencia = a;
  }

  public void setHorasAsignadas(Integer a) {
      this.HorasAsignadas = a;
  }

  public SimpleDateFormat getfechaAlta() {
      return this.fechaAlta;
  }

  public void setfechaAlta(SimpleDateFormat a) {
      this.fechaAlta = a;
  }
  
  public Grupo getGrupo() {
      return this.grup;
  }

  public void setGrupo(Grupo a) {
      this.grup = a;
  }

}